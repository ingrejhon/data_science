**********************ACESOS all_zoneES --- 510 all_zoneES**********************************

--Zona peatonal carrera 106
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,3,1 from authorizations a where all_zone  = 1;

--Zona peatonal calle 13
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,4,1 from authorizations a where all_zone  = 1;

--Motos
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,5,1 from authorizations a where all_zone  = 1;

--Ciclas
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,6,1 from authorizations a where all_zone  = 1;

--Vehicular Principal
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,7,1 from authorizations a where all_zone  = 1;


--Vehicular Operaciones
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,8,1 from authorizations a where all_zone  = 1;

--Taller
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,9,1 from authorizations a where all_zone  = 1;

--Edificio Administrativo 1 Piso
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,10,1 from authorizations a where all_zone  = 1;

--Edificio Administrativo 2 Piso
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,11,1 from authorizations a where all_zone  = 1;

--Entrada Central de Monitoreo
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,12,1 from authorizations a where all_zone  = 1;


**********************ACESOS EXTERIORES --- 30.513 all_zoneES**********************************

--Zona peatonal carrera 106
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,3,1 from authorizations a where all_zone  = 0;

--Zona peatonal calle 13
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,4,1 from authorizations a where all_zone  = 0;

--Motos
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,5,1 from authorizations a where all_zone  = 0;

--Ciclas
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,6,1 from authorizations a where all_zone  = 0;

--Vehicular Principal
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,7,1 from authorizations a where all_zone  = 0;


--Vehicular Operaciones
insert into authorization_zones(authorization_id ,zone_id , state )
select id as authorization_id,8,1 from authorizations a where all_zone  = 0;



**************************DIAS!!!!!!!!!!******************************************************
SELECT id, authorization_id, day_id, state, created_by, updated_by, created_at, updated_at
FROM public.authorization_days where authorization_id=3;

insert into authorization_days (authorization_id, day_id , state, created_by ,created_at)
select id,7,1,1,NOW() from authorizations a;